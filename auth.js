const jwt = require('express-jwt');
const decode = require('jwt-decode');
const jwksRsa = require('jwks-rsa');
const request = require('request');
const debug = require('debug');
const Config = require('config');

const log = debug('berryapi');
const auth0 = Config.get('auth0.server');

function certToPEM(cert) {
    cert = cert.match(/.{1,64}/g).join('\n');
    cert = `-----BEGIN CERTIFICATE-----\n${cert}\n-----END CERTIFICATE-----\n`;
    return cert;
}

const checkJwt = jwt({
    secret: jwksRsa.expressJwtSecret({
        cache: true,
        rateLimit: true,
        jwksRequestPerMinutes: 5,
        jwksUri: auth0.jwksUri
    }),
    audience: auth0.audience,
    issuer: auth0.issuer,
    algorithms: ['RS256']
});

const checkSocketJwt = (data, next) => {
    try {
        if (!data.handshake.query.token) {
            throw Error('No token');
        }
        const decoded = decode(data.handshake.query.token);
        const header = decode(data.handshake.query.token, {header: true});
        const kid = header.kid;
        log(header);
        log(decoded);
        request({
            json: true,
            uri: auth0.jwksUri,
            strictSsl: true
        }, (err, res) => {
            if(!res.body.keys) {
                throw Error('No body keys');
            }
            const signKeys = res.body.keys.filter(key => key.use === 'sig' && key.kty === 'RSA' && key.kid && ((key.x5c && key.x5c.length)))
                .map(key => {
                    if(key.x5c && key.x5c.length) {
                        return {
                            kid: key.kid,
                            nbf: key.nbf,
                            publicKey: certToPEM(key.x5c[0])
                        };
                    }
                });
            const k = signKeys.find(k => k.kid === kid);
            if(k) {
                log('Success, kid ok');
                next(null, k);
            } else {
                throw new Error('Kid not found in keys');
            }
        });
    } catch (e) {
        log('Fail: no token ', e);
        next(e, false);
    }
}

module.exports = {checkJwt, checkSocketJwt};