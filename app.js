const express = require('express');
const path = require('path');
const debug = require('debug');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const Config = require('config');
const index = require('./routes/index');
const cors = require('cors');
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);
const checkSocketJwt = require('./auth').checkSocketJwt;
const log = debug('reportapi');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(cors({origin: Config.get('allowedOrigins')}));
app.use(express.static(path.join(__dirname, 'public')));
app.use((req, res, next) => {
    res.io = io;
    next();
});

const wsConfig = Config.get('events');

require('./routes/ws')(io, checkSocketJwt, wsConfig, log);


app.use('/', index);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    const err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error.html');
});

module.exports = {app, server};
module.exports.io = io;
