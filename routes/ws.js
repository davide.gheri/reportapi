const si = require('systeminformation');

const init = (io, checkSocketJwt, config, log) => {
    async function os() {
        if (config.os) {
            const os = await si.osInfo(),
                cpu = await si.cpu(),
                users = await si.users();
            io.emit(config.os, {os, cpu, users});
        }
    }
    async function cpu() {
        if (config.cpu) {
            const current = await si.cpuCurrentspeed();
            const cpu = await si.cpu();
            const load = await si.currentLoad();
            io.emit(config.cpu, {cpu, current, load});
        }
    }
    async function fs() {
        if (config.filesystem) {
            const fs = await si.fsSize();
            const IO = await si.disksIO();
            const disks = fs.filter((disk) => disk.mount !== '/boot'); //Hide recovery partition
            io.emit(config.filesystem, {disks, IO, disk: fs[0]});
        }
    }
    async function ram() {
        if (config.ram) {
            const ram = await si.mem();
            io.emit(config.ram, {ram});
        }
    }
    async function temp() {
        if (config.temperature) {
            const temp = await si.cpuTemperature();
            io.emit(config.temperature, {temperature: temp.main});
        }
    }
    async function procs() {
        if (config.processes) {
            const processes = await si.processes();
            io.emit(config.processes, {processes});
        }
    }
    const emitAll = () => {
        os();
        cpu();
        fs();
        ram();
        temp();
        procs();
    };
    let interval;
    let emitting = false;
    let connections = 0;
    io.use(checkSocketJwt);
    io.on('connection', conn => {
        log('Welcome');
        if(!emitting && connections == 0) {
            emitAll();
            interval = setInterval(emitAll, 5000);
            emitting = true;
            log('Started emitters');
        }
        connections = connections + 1;
        conn.on('disconnect', () => {
            log('Bye');
            connections = connections - 1;
            if(emitting && connections == 0) {
                log('Stopped emitters');
                clearInterval(interval);
                emitting = false;
            }
        });
    });
};

module.exports = init;